/**
 * connection wrapper
 */

package tok

import (
	"errors"
	"log"
	"sync"
	"sync/atomic"
	"time"

	"bitbucket.org/ilifei/msn/logger"
	"bitbucket.org/ilifei/msn/version"
)

var (
	// HeartInterval client ping timeout duration
	HeartInterval = 10 * time.Second
	//ReadTimeout read timeout duration
	ReadTimeout time.Duration
	//WriteTimeout write timeout duration
	WriteTimeout = time.Minute
	//AuthTimeout auth timeout duration
	AuthTimeout = time.Second * 5
)

//abstract connection,
type connection struct {
	sync.RWMutex
	wLock   sync.Mutex
	dv      *Device
	adapter conAdapter
	hub     *Hub
	closed  bool

	counter int32
}

type conState struct {
	con    *connection
	online bool
}

//conAdapter is adapter for real connection.
//For now, net.Conn and websocket.Conn are supported.
//This interface is useful for building test application
type conAdapter interface {
	Read() ([]byte, error) //Read payload data from real connection. Unpack from basic data frame
	Write([]byte) error    //Write payload data to real connection. Pack into basic data frame
	Close()                //Close the real connection
}

func (conn *connection) uid() interface{} {
	return conn.dv.UID()
}

func (conn *connection) isClosed() bool {
	conn.RLock()
	defer conn.RUnlock()
	return conn.closed
}

func (conn *connection) readLoop() {
	for {
		if conn.isClosed() {
			return
		}

		b, err := conn.adapter.Read()
		if err != nil {
			log.Println("read err", err)
			conn.hub.stateChange(conn, false)
			return
		}
		// when a pack up, counter increment
		atomic.AddInt32(&conn.counter, 1)
		conn.hub.receive(conn.dv, b)
	}
}

func (conn *connection) close() {
	conn.Lock()
	defer conn.Unlock()

	conn.closed = true
	conn.adapter.Close()
}

func (conn *connection) Write(b []byte) error {
	conn.wLock.Lock()
	defer conn.wLock.Unlock()

	if conn.isClosed() {
		return errors.New("Can't write to closed connection")
	}

	if err := conn.adapter.Write(b); err != nil {
		conn.hub.stateChange(conn, false)
		return err
	}
	return nil
}

func initConnection(dv *Device, adapter conAdapter, hub *Hub) {
	conn := &connection{
		dv:      dv,
		adapter: adapter,
		hub:     hub,
		counter: -1,
	}

	hub.stateChange(conn, true)

	// heart bit checker
	ticker := time.NewTicker(3 * HeartInterval)
	go func() {
		for range ticker.C {
			if conn.isClosed() {
				ticker.Stop()
				return
			}

			// when 3 times not a ping signal up, close this conn
			if conn.counter == 0 {
				logger.Warning.Printf("node[%s] 3 heart beat times not up data, close current conn", version.NodeId)
				hub.Kick(dv.UID())
			} else {
				atomic.SwapInt32(&conn.counter, 0)
			}
		}
	}()

	//block on read
	conn.readLoop()
}
