/**
 * websocket connection adapter
 */

package tok

import (
	"golang.org/x/net/websocket"
	"log"
	"net/http"
	"time"
)

type wsAdapter struct {
	conn *websocket.Conn
	txt  bool
}

func (p *wsAdapter) Read() ([]byte, error) {
	if ReadTimeout > 0 {
		if err := p.conn.SetReadDeadline(time.Now().Add(ReadTimeout)); err != nil {
			log.Println("[warning] setting ws read deadline: ", err)
			return nil, err
		}
	}

	if p.txt {
		var s string
		err := websocket.Message.Receive(p.conn, &s)
		return []byte(s), err
	}

	var b []byte
	err := websocket.Message.Receive(p.conn, &b)
	return b, err

}

func (p *wsAdapter) Write(b []byte) error {
	if err := p.conn.SetWriteDeadline(time.Now().Add(WriteTimeout)); err != nil {
		log.Println("[warning] setting ws write deadline: ", err)
		return err
	}

	if p.txt {
		return websocket.Message.Send(p.conn, string(b))
	}

	return websocket.Message.Send(p.conn, b)
}

func (p *wsAdapter) Close() {
	p.conn.Close()
}

//CreateWsHandler create web socket http handler with hub.
//If config is not nil, a new hub will be created and replace old one
//If txt is true web socket will serve text frame, otherwise serve binary frame
//auth function is used for user authorization
//Return http handler
func CreateWsHandler(hub *Hub, config *HubConfig, txt bool, auth TCPAuthFunc) (*Hub, http.Handler) {
	if config != nil {
		hub = createHub(config.Actor, config.Q, config.Sso)
	}

	if hub == nil {
		log.Fatal("hub is needed")
	}

	return hub, websocket.Handler(func(ws *websocket.Conn) {
		adapter := &wsAdapter{conn: ws, txt: txt}

		b, err := adapter.Read()
		if err != nil {
			log.Println("[error] ws auth err ", err)
			b := responseErrorHandle(4006, "tcp auth err")
			adapter.Write(b)
			adapter.Close()
			return
		}

		dv, info, err := auth(b)
		if err != nil {
			log.Printf("[error] request server error: %v", err)
			adapter.Write(info)
			adapter.Close()
			return
		}

		if info != nil {
			log.Printf("[info] sent to client % x\n", info)
			adapter.Write(info)
		}

		initConnection(dv, adapter, hub)
	})
}

//WsAuthFunc websocket auth function, return Device interface
//parameter is the initial websocket request
type WsAuthFunc func(*http.Request) (*Device, []byte, error)
